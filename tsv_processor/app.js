const express = require('express')
const morgan = require('morgan')
const bodyParser = require('body-parser')

const app = express()

app.use(morgan('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

const parseHandler = require('./parseHandler')
const idsHandler = require('./idsHandler')

app.post('/parse', parseHandler)
app.get('/ids', idsHandler)

module.exports = app
