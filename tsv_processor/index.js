require('dotenv').config()

const cluster = require('cluster')
const app = require('./app')

const workersCount = parseInt(process.env.WORKERS) || 0
const port = parseInt(process.env.PORT) || 8000

function start() {
  app.listen(port, () => console.log(`Listening on port ${port}`))
}

function singleMode() {
  console.log('Starting server in Single mode...')
  start()
}

function clusterMode() {
  if (cluster.isMaster) {
    console.log('Starting server in Cluster mode...')
    for (let i = 0; i < workersCount; i++) {
      cluster.fork()
    }
  } else {
    console.log(`Booting worker #${cluster.worker.id}`)
    start()
  }
}

if (workersCount === 0) {
  singleMode()
} else {
  clusterMode()
}
