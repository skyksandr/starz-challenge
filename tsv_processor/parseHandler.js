const { promisify } = require('util')
const zlib = require('zlib')
const request = require('request')

const redisClient = require('./redisClient')

const getAsync = promisify(request.get)
const gunzipAsync = promisify(zlib.gunzip)

module.exports = function(req, res) {
  // encoding: null to get the body as a buffer
  // src: https://github.com/request/request/issues/57#issuecomment-2666032
  getAsync(req.body.location, { encoding: null })
    .then(response => gunzipAsync(response.body))
    .then(buffer => buffer.toString())
    .then(tsv => extractIds(tsv))
    .then(ids => storeIds(ids))
    .then(() => res.send(JSON.stringify({status: 'OK'})))
    .catch(err => res.send(JSON.stringify({error: err})))
}

function extractIds(tsv) {
  let [_header, ...entries] = tsv.split("\n")

  let ids = entries
    .map(row => row.split("\t")[0])
    .filter(id => id.length > 0)

  return ids
}

function storeIds(ids) {
  const chunkSize = 1000

  while(ids.length > 0) {
    redisClient.sadd('imdb:ids', ids.splice(0, chunkSize))
  }
}
