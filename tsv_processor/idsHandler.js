const { promisify } = require('util')
const redisClient = require('./redisClient')
const limit = parseInt(process.env.FETCH_LIMIT)

const spopAsync = promisify(redisClient.spop).bind(redisClient)

module.exports = function(req, res) {
  const recordsCount = parseInt(req.query.limit) || limit

  spopAsync('imdb:ids', recordsCount)
    .then(records => res.send(JSON.stringify(records)))
    .catch(err => res.send(JSON.stringify({error: err})))
}
