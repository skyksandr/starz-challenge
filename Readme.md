### Prerequisites

You have `docker` and `docker-compose`

### Configuration

TSV service:

```
 - REDIS_URL
 - FETCH_LIMIT - default limit for ids endpoint
```

OMDB:
```
  - REDIS_URL
  - OMDB_CONCURRENCY - max parallel omdb requests, default 10
  - OMDB_API_KEY - Your omdb api key
  - TSV_URL - url to service to query ids for fetch from
```

### Use

1. git clone the repo
2. run `docker-compose up` from the root directory of this repo

#### Load test slice of original

```
cd test_datasets && yarn start

curl -X POST localhost:8000/parse \
  -H "Content-Type: application/json" \
  --data '{"location": "http://localhost:8080/title.akas.mini.tsv.gz"}'
```

#### Get meta

```
curl 'localhost:9000/meta/tt0000008'

{
  "id":"tt0000008",
  "title":"Edison Kinetoscopic Record of a Sneeze",
  "genre":["Documentary","Short"],
  "actors":["Fred Ott"],
  "description":"A man (Thomas Edison's assistant) takes a pinch of snuff and sneezes. This is one of the earliest Thomas Edison films and was the first motion picture to be copyrighted in the United States.",
  "image":"https://images-na.ssl-images-amazon.com/images/M/MV5BNzFkN2JkNjEtZDZhNi00NmVmLTg4YWUtOGMzZWQ4NzFmZWI0XkEyXkFqcGdeQXVyMDEyNzY3OA@@._V1_SX300.jpg",
  "imdbRating":"5.6",
  "metaRating":"N/A"
}

```

#### Load full 3M+ records dataset

```
curl -X POST localhost:8000/parse \
  -H "Content-Type: application/json" \
  --data '{"location": "https://datasets.imdbws.com/title.akas.tsv.gz"}'
```
