const { promisify } = require('util')
const express = require('express')
const morgan = require('morgan')

const redisClient = require('./redisClient')
const getAsync = promisify(redisClient.get).bind(redisClient)

const present = require('./metaPresenter')

const app = express()
app.use(morgan('dev'))

app.get('/meta/:id', (req, res) => {
  getAsync(`meta:${req.params.id}`)
    .then(data => JSON.parse(data))
    .then(present)
    .then(json => res.send(JSON.stringify(json)))
    .catch(err => res.send(JSON.stringify({error: err})))
})

module.exports = app
