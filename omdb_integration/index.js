require('dotenv').config()
const app = require('./app')
const sync = require('./sync')

const port = parseInt(process.env.PORT) || 8000

console.log('Starting server in Single mode...')
app.listen(port, () => console.log(`Listening on port ${port}`))

console.log('Starting sync loop...')
sync.start()
