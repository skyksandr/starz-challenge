const { promisify } = require('util')
const async = require('async')
const request = require('request')
const redisClient = require('./redisClient')
const getAsync = promisify(request.get)

const concurrency = parseInt(process.env.OMDB_CONCURRENCY) || 10

const worker = function(imdbId, callback) {
  const url = `http://www.omdbapi.com/?apikey=${process.env.OMDB_API_KEY}&i=${imdbId}&tomatoes=true`

  getAsync(url)
    .then(response => response.body)
    .then(json => redisClient.set(`meta:${imdbId}`, json))
    .then(json => redisClient.rpush('meta:available', imdbId))
    .then(callback)
    .catch(err => console.log(err))
}

const queue = async.queue(worker, concurrency)

let inteval = 1000

function fetchWithInterval() {
  const url = `${process.env.TSV_URL}/ids`

  getAsync(url)
    .then(response => JSON.parse(response.body))
    .then(collection => {
      if (collection.length === 0) {
        interval = 1000
        return
      } else {
        interval = 300
        collection.forEach(el => queue.push(el))
      }
    })
    .then(() => setTimeout(fetchWithInterval, interval))
}

function start() {
  fetchWithInterval()
}

module.exports.start = start
