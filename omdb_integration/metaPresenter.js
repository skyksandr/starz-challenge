function present(data) {
  return {
    id: data.imdbID,
    title: data.Title,
    genre: data.Genre.split(',').map(el => el.trim()),
    actors: data.Actors.split(',').map(el => el.trim()),
    description: data.Plot,
    image: data.Poster,
    imdbRating: data.imdbRating,
    metaRating: data.Metascore,
    rottenTomatoesRating: data.tomotoRating
  }
}

module.exports = present
